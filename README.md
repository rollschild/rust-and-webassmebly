# Rust and WebAssembly

## Why Rust and WebAssmebly

- small `.wasm` sizes
  - Rust lacks a runtime -> small `.wasm` sizes

## WebAssmebly

- simple machine model and executable format
- designed to be portable, compact, and execute at/near native speeds
- two formats:
  - `.wat` (**W**eb**A**ssembly**T**ext) text format
    - uses **S-expressions**
  - `.wasm` binary format
    - lower-level
    - intended for consumption directly by wasm virtual machines
    - similar to ELF and Mach-O

### Modules

- WebAssembly programs are organized into modules
  - which, are the unit of deployment, loading, and compilation
- module 
  - collects definitions for types, functions, tables, and globals
  - declares imports and exports
  - provides initialization in the form of data and element segments, or
  a start function
- entire module may be empty

#### Indices

- definitions are referenced with zero-based **indices**
- each class of definition has its own index **space**
- index space for functions, tables, memories, and globals includes imports
declared in the same module
  - indices of these imports precede the indices of other definitions in the
  same index space
- element indices reference element segments
- data indices reference data segments
- index space for locals only accessible inside a function
  - includes the parameters of that function,
  - which precede the local variables
- label indices reference **structured control instructions** inside an
instruction sequence
- Conventions
  - meta variable _l_ ranges over label indices
  - meta variable _x_, _y_, range over indices in any of the other index spaces
  - notation `idx(A)`: set of indices from index space _idx_ occurring free in
  _A_
    - sometimes we reinterpret this set as the **vector** of its elements

#### Types

- the **types** component of a module defines a vector of **function types**
- all function types used in a module must be defined in this component
  - referenced by [type indices](#indices)

#### Functions

- the **funcs** component defines a vector of _functions_
- **type** of a function declares its signature by reference to a **type**
defined in the module
- parameters of the function are referenced through 0-based [local
indices](#indices)
in the function's body
  - they are _mutable_
- **locals** declare a vector of mutable local variables and their types
  - referenced through [local indices](#indices) in the function's body
  - index of first local is the smallest index not referencing a parameter
- **body** is an **instruction** sequence
- functions are referenced through function indices
  - starting with smallest index not referencing a function **import**

#### Tables

- a vector of opaque values of a particular **reference type**
- can be initialized through **element segments**
- referenced through [table indices](#indices)

#### Memories

- **mems** component defines a vector of **linear memories**
- **memory type**
- memory is a vector of **raw uninterpreted bytes**
- initialized through **data segments**
- referenced through [memory indices](#indices)
- currently, at most one memory may be defined/imported in a single module
  - _all_ constructs implicitly reference this memory `0`

#### Globals

- its type specifies whether a global is immutable or mutable
- each global initialized with an **init** value given by a **constant**
**initializer expression**

#### Element Segments

- initial contents of a table is _uninitialized_
- element segments can be used to initialize a subrange of a table from
a static **vector** of elements
- **elems** component
  - each element segment defines: 
    - a **reference type**
    - a corresponding list of **constant** **element expressions**
- a mode that identifies element segments as either **passive**, **active**, or
**declarative**
- currently only tables of element type **funcref** can be initialized with an
element segment

#### Data Segments

- initial contents of a [memory](#memories) are zero bytes
- **data segments** can be used to initialize a range of memory from a static
vector of bytes
- a mode that identifies them as either _passive_ or _active_
- referenced through [data indices](#indices)
- currently at most one memory allowed in a moduel
  - the only valid **memidx** is `0`

#### Start Function

- **start** component declares the [function index](#indices) of a **start
function** that is automatically invoked when the module is **instantiated**
  - after [tables](#tables) and [memories](#memories) have been initialized
- start function is intended for initializing the state of a module
  - the module and its exports are not accessible before this initialization
  has completed

#### Exports

- **exports** component defines a set of _exports_ that become accessible to
the _host environment_ once the module has been **instantiated**
- exportable definitions:
  - functions
  - tables
  - memories
  - globals

#### Imports

- **imports** component defines a set of imports that are _required_ for
**instantiation**
- each import is labeled by a two-level name space
  - a **module** name
  - a name for an entity within that module
- every import defines an index in the respective [index space](#indices)
- the indices of imports go before the first index of _any_ definition
contained in the module itself
- import names are not necessarily unique

### Linear Memory

- a simple [memory model](#memories)
- a wasm module has access to a single _linear memory_,
  - which is a flat array of bytes
- this memory can be grown by a multiple of the page size, 64K
- **cannot** be shrunk
